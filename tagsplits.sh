#!/bin/sh
#------------------------------------------------------------------------------
#
# tagsplits.sh: create matching tags across the split repositories.
#
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Environment variables and semi-constants.

# Default timezone for dates in logs
: "${TZ:=MST7}"

# Main branch name
: "${UAQSRTOOLS_MAINBRANCH:=7.x-1.x}"

# Stem for writable repository URLs
: "${UAQSRTOOLS_REPOSTEM:=git@bitbucket.org:ua_drupal/}"

# Suffix for writable repository URLs
: "${UAQSRTOOLS_REPOSUFFIX:=.git}"

# Directory holding the monorepo clone
: "${UAQSRTOOLS_MONOCLONE:=$PWD}"

# Relative paths to the subtrees of interest
: "${UAQSRTOOLS_PATHLIST:=.}"

# Splitting utility name
: "${UAQSRTOOLS_SPLITTER:=lite}"

# Prefix for messages on commits to be tagged
: "${UAQSRTOOLS_MAGICTEXT:=Preparing to tag }"

# Name for the monorepo
: "${UAQSRTOOLS_OURNAME:=ua_quickstart}"

# Short Git remote name for the remote monorepo
: "${UAQSRTOOLS_MONOREMOTE:=origin}"

# Executable path
if [ -z "$UAQSRTOOLS_EXEPATH" ]; then
  if [ -n "$GOPATH" ]; then
    UAQSRTOOLS_EXEPATH="${GOPATH}/bin/"
  fi
fi

#------------------------------------------------------------------------------
# Utility functions definitions.

errorexit () {
  echo "** $1." >&2
  exit 1
}

# Show progress on STDERR, unless explicitly quiet.
if [ -z "$UAQSRTOOLS_QUIET" ]; then
  logmessage () {
    echo "$1..." >&2
  }
  normalexit () {
    echo "$1." >&2
    exit 0
  }
else
  logmessage () {
    return
  }
  normalexit () {
    exit 0
  }
fi

#------------------------------------------------------------------------------
# Main processing function definitions.

# Iterate over the paths within the monorepo containing subtrees.
#
# Parameters:
# $1 - the operation to perform (function).
# $2 - operation name, for logging messages (string).
mappaths () {
  operation="$1"
  opname="$2"
  for relpath in $UAQSRTOOLS_PATHLIST ; do
    [ -d "$relpath" ] \
      || errorexit "${opname}: cannot find a directory holding subtrees at ${relpath}"
    logmessage "${opname}: looking within ${relpath} for directories holding subtrees"
    # Assume that any directory found holds a subttree to be processed
    for splitprefix in "${relpath}/"* ; do
      if [ -d "$splitprefix" ]; then
        manyname=$(basename "$splitprefix")
        sha=$("${UAQSRTOOLS_EXEPATH}${UAQSRTOOLS_SPLITTER}" --prefix="$splitprefix" --target="refs/heads/${manyname}") \
          || errorexit "${UAQSRTOOLS_SPLITTER} crashed with status ${?} when splitting ${splitprefix}"
        [ -n "$sha" ] \
          || "Could not split ${splitprefix} from the monorepo"
        logmessage "Split ${splitprefix} from the monorepo as ${sha}"
        # Perform the operation on the split subtree
        $operation "$splitprefix" "$manyname"
      fi
    done
  done
}

# Update a CHANGELOG.txt file from subtree commit messages.
#
# Parameters:
# $1 - the subtree path, from the repository root (string).
# $2 - the branch name (string).
updatechangelog () {
  oldchangelog="$1/CHANGELOG.txt"
  branchname="$2"
  branchref="refs/heads/$2"
  git rev-parse --verify --quiet "$branchref" \
    || errorexit "Could not find the branch ${branchname} for the changelog"
  [ -e "$oldchangelog" ] \
    || errorexit "Could not find the old changelog at ${oldchangelog}"
  newchangelog=$(mktemp "${scratchdir}/logtmp.XXXXXX") \
    || errorexit "Could not make the temporary changelog in ${scratchdir}"
  { printf "%s" "${UAQSRTOOLS_NEXTTAG}, " \
      && date +%F \
      && echo '-----------------------------'
  } >> "$newchangelog" \
    || errorexit "Could not write the new changelog heading to ${newchangelog}"
  { git log "$branchref" --pretty=%s \
      | awk "$global_awk_filter_script"
  } >> "$newchangelog" \
    || errorexit "Failed when filtering Git commit messages for the changelog"
  cat "$oldchangelog" >> "$newchangelog" \
    || errorexit "Could not append the old changelog ${oldchangelog} to the new one at ${newchangelog}"
  mv "$newchangelog" "$oldchangelog" \
    || errorexit "Could not revise the changelog at ${oldchangelog}"
  commitmessage="${UAQSRTOOLS_MAGICTEXT}${UAQSRTOOLS_NEXTTAG} for ${branchname}."
  git commit -a -m "$commitmessage" \
    || "Could not commit '${commitmessage}'"
  logmessage "Committed '${commitmessage}'"
}

# Push an updated split.
#
# Parameters:
# $1 - the repository to hold the split (string).
# $2 - the subtree name (string).
pushsplit () {
  splitrepo="$1"
  subtreename="$2"
  git push -f "$splitrepo" "+${subtreename}:${UAQSRTOOLS_MAINBRANCH}" \
    || errorexit "Could not push the split for ${subtreename} to ${splitrepo}"
  logmessage "Pushed the split for ${subtreename} to ${splitrepo}"
}

# Move or create a tag and push it.
#
# Parameters:
# $1 - the repository to receive the tag (string).
# $2 - the branch name (string).
pushtag () {
  destrepo="$1"
  branchname="$2"
  tagmessage="${branchname} ${UAQSRTOOLS_NEXTTAG}"
  git tag -f -a "$UAQSRTOOLS_NEXTTAG" -m "$tagmessage" "$branchname" \
    || errorexit "Could not move the tag ${tagmessage}"
  logmessage "Moved the tag ${tagmessage}"
  git push -f "$destrepo" "$UAQSRTOOLS_NEXTTAG" \
    || errorexit "Could not push the tag ${tagmessage} to ${destrepo}"
  logmessage "Pushed the tag ${tagmessage} to ${destrepo}"
}

# Push the local splits to the manyrepos, tag them, and push the tags.
#
# Parameters:
# $1 - the subtree path, from the repository root (string).
# $2 - the subtree split name (string).
pushchanges () {
  logmessage "Pushing changes in $1"
  splitname="$2"
  manyrepo="${UAQSRTOOLS_REPOSTEM}${splitname}${UAQSRTOOLS_REPOSUFFIX}"
  pushsplit "$manyrepo" "$splitname"
  pushtag "$manyrepo" "$splitname"
}

#------------------------------------------------------------------------------
# Initial run-time error checking.

# Directory checks.
[ -d "$UAQSRTOOLS_MONOCLONE" ] \
  || errorexit "The monorepo clone directory ${UAQSRTOOLS_MONOCLONE} does not exist"
cd "$UAQSRTOOLS_MONOCLONE"
git status \
  || errorexit "No repository in the ${UAQSRTOOLS_MONOCLONE} directory"
logmessage "In the monorepo Git repository directory ${UAQSRTOOLS_MONOCLONE}"

# Changes applied to the monorepo will be on this specific branch.
git checkout "$UAQSRTOOLS_MAINBRANCH" \
  || errorexit "Failed to check out the monorepo branch ${UAQSRTOOLS_MAINBRANCH}"

# Basic sanity checks: are the tags defined?
[ -z "$UAQSRTOOLS_NEXTTAG" ] \
  && UAQSRTOOLS_NEXTTAG=$(git describe --tags --always --abbrev=0)
[ -n "$UAQSRTOOLS_NEXTTAG" ] \
  || errorexit "No new tag defined"
logmessage "Applying the new tag ${UAQSRTOOLS_NEXTTAG}"

# Is there already a commit at HEAD with a message referencing the tag?
messagetag=$(git log -1 --pretty=%s HEAD | sed "s/^${UAQSRTOOLS_MAGICTEXT}\\([^ ]*\\) .*\$/\\1/") \
  || errorexit "Failed when checking the Git history for previous work on the ${UAQSRTOOLS_NEXTTAG} tag"
[ "$messagetag" != "$UAQSRTOOLS_NEXTTAG" ] \
  || normalexit "The changes for the tag ${UAQSRTOOLS_NEXTTAG} have already been applied"

# An explicit old tag overrides the nearest existing tag before the current HEAD.
[ -n "$UAQSRTOOLS_LASTTAG" ] \
  || UAQSRTOOLS_LASTTAG=$(git describe --tags --always --abbrev=0 HEAD~ ) \
  || errorexit "Failed when trying to identify the previous tag"
[ -n "$UAQSRTOOLS_LASTTAG" ] \
  || errorexit "No old tag defined for building the change logs"
logmessage "Change logs will be updated back to tag ${UAQSRTOOLS_LASTTAG}"

#------------------------------------------------------------------------------
# Scratch directory for assembling revised changelogs.

scratchdir=$(mktemp -d -t "uaqsrtools_scratch_XXXXXX") \
  || errorexit "Could not make a scratch directory"
trap 'rm -Rf "${scratchdir}"' EXIT TERM INT QUIT

#------------------------------------------------------------------------------
# Make an awk script that extracts Git log commit messages back to a tag.

filter_begin="
BEGIN {
  tagpattern = \"^${UAQSRTOOLS_MAGICTEXT}${UAQSRTOOLS_LASTTAG}\"
}
"
# shellcheck disable=SC2016
filter_lines='
$0 !~ tagpattern { print "- "$0; }
$0  ~ tagpattern { print "" ; exit }
'
global_awk_filter_script="${filter_begin}${filter_lines}"

#------------------------------------------------------------------------------
# Process eveything.

# Update, tag, and push the splits to the manyrepos.
mappaths updatechangelog "Updating the change log"
mappaths pushchanges "Pushing the changes"

# Update and tag the monorepo.
updatechangelog "." "$UAQSRTOOLS_MAINBRANCH"
git push -f "$UAQSRTOOLS_MONOREMOTE" "$UAQSRTOOLS_MAINBRANCH" \
  || errorexit "Failed to push the updated change logs back to ${UAQSRTOOLS_MONOREMOTE}"
pushtag "$UAQSRTOOLS_MONOREMOTE" "$UAQSRTOOLS_MAINBRANCH"

normalexit "Finished processing the changes for the tag ${UAQSRTOOLS_NEXTTAG}"
