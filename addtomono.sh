#!/bin/sh
#------------------------------------------------------------------------------
#
# addtomono.sh: add new subtrees from freestanding repositories.
#
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Environment variables and semi-constants.

# Default timezone for dates in logs
: "${TZ:=MST7}"

# Main branch name
: "${UAQSRTOOLS_MAINBRANCH:=7.x-1.x}"

# Stem for writable repository URLs
: "${UAQSRTOOLS_REPOSTEM:=git@bitbucket.org:ua_drupal/}"

# Suffix for writable repository URLs
: "${UAQSRTOOLS_REPOSUFFIX:=.git}"

# Directory holding the monorepo clone
: "${UAQSRTOOLS_MONOCLONE:=$PWD}"

# Relative paths to the subtrees of interest
: "${UAQSRTOOLS_PATHLIST:=.}"

# Prefix for messages on commits to be tagged
: "${UAQSRTOOLS_ADDPRFIX:=ADDTOMONO}"

# Name for the monorepo
: "${UAQSRTOOLS_OURNAME:=ua_quickstart}"

# Short Git remote name for the remote monorepo
: "${UAQSRTOOLS_MONOREMOTE:=origin}"

# Count of placeholder substitutions
global_subst_count=0

#------------------------------------------------------------------------------
# Utility functions definitions.

errorexit () {
  echo "** $1." >&2
  exit 1
}

# Show progress on STDERR, unless explicitly quiet.
if [ -z "$UAQSRTOOLS_QUIET" ]; then
  logmessage () {
    echo "$1..." >&2
  }
  normalexit () {
    echo "$1." >&2
    exit 0
  }
else
  logmessage () {
    return
  }
  normalexit () {
    exit 0
  }
fi

#------------------------------------------------------------------------------
# Main processing function definitions.

# Search and replace for placeholder files.
#
# Parameters:
# $1 - the path to the directory containing the placeholders (module root).
substplaceholders () {
  modhome="$1"
  for pholder in "${modhome}/${UAQSRTOOLS_ADDPRFIX}_"* ; do
    if [ -f "$pholder" ]; then
      manyname=$(basename "$pholder" | sed "s/${UAQSRTOOLS_ADDPRFIX}_//" )
      [ -n "$manyname" ] \
        || errorexit "Cannot make a repository name from the placeholder"
      git rm "$pholder" \
        || errorexit "Could not remove the ${pholder} placeholder file"
      logmessage "Removed the ${pholder} placeholder file"
      git commit -m "Delete the placeholder file for the ${manyname} subtree." \
        || errorexit "Could not delete the ${manyname} subtree placeholder"
      git subtree add --prefix="${modhome}/${manyname}" "${UAQSRTOOLS_REPOSTEM}${manyname}${UAQSRTOOLS_REPOSUFFIX}" HEAD \
        || errorexit "Could not add the subtree for the module repository ${manyname}"
      logmessage "Added the remote manyrepo module repository ${manyname} to the monorepo"
      global_subst_count=$((global_subst_count+1))
    fi
  done
}

# Iterate over the paths within the monorepo containing subtrees.
scanpaths () {
  for relpath in $UAQSRTOOLS_PATHLIST ; do
    [ -d "$relpath" ] \
      || errorexit "Cannot find a directory holding subtree placeholder files at ${relpath}"
    logmessage "Looking within ${relpath} for subtree placeholder files"
    substplaceholders "$relpath"
  done
}

#------------------------------------------------------------------------------
# Initial run-time error checking.

# Directory checks.
[ -d "$UAQSRTOOLS_MONOCLONE" ] \
  || errorexit "The monorepo clone directory ${UAQSRTOOLS_MONOCLONE} does not exist"
cd "$UAQSRTOOLS_MONOCLONE"
git status \
  || errorexit "No repository in the ${UAQSRTOOLS_MONOCLONE} directory"
logmessage "In the monorepo Git repository directory ${UAQSRTOOLS_MONOCLONE}"

# The placeholder file additions must be on the working branch.
[ -n "$UAQSRTOOLS_WORKINGBRANCH" ] \
  || errorexit "No working branch defined."
git checkout "$UAQSRTOOLS_WORKINGBRANCH" \
  || errorexit "Failed to check out the monorepo branch ${UAQSRTOOLS_WORKINGBRANCH}"

#------------------------------------------------------------------------------
# Process eveything.

scanpaths

# If there are no substitutions, do nothing (not even branch deletion).
[ "$global_subst_count" -ne 0 ] \
  || errorexit "No subtree substitutions made on the ${UAQSRTOOLS_WORKINGBRANCH} branch"

# Up to now nothing has touched the main repository: destructively push changes.
git push -f "$UAQSRTOOLS_MONOREMOTE" "+${UAQSRTOOLS_WORKINGBRANCH}:${UAQSRTOOLS_MAINBRANCH}" \
  || errorexit "Failed to push changes from the local ${UAQSRTOOLS_WORKINGBRANCH} branch up to the ${UAQSRTOOLS_MAINBRANCH} branch in ${UAQSRTOOLS_MONOREMOTE}"
logmessage "Pushed the changes up to the ${UAQSRTOOLS_MAINBRANCH} branch in ${UAQSRTOOLS_MONOREMOTE}"

git push "$UAQSRTOOLS_MONOREMOTE" --delete "$UAQSRTOOLS_WORKINGBRANCH" \
  || errorexit "Failed to delete the ${UAQSRTOOLS_WORKINGBRANCH} branch from ${UAQSRTOOLS_MONOREMOTE}"
logmessage "Deleted the ${UAQSRTOOLS_WORKINGBRANCH} branch"

normalexit "Finished processing the changes on ${UAQSRTOOLS_WORKINGBRANCH}"
