# UAQS Repository Tools

Meta-tools for manipulating the repositories that the UA Quickstart Drupal
distribution uses.

In particular, these scripts handle the splitting of a subtrees within a single
single large Git repository (_monorepo_) into many freestanding repositories
(_manyrepos_) using [splitsh-lite](https://github.com/splitsh/lite) and similar
tools. Some of the scripts also handle the reverse operation, placing an
existing freestanding repository as a subtree within the _monorepo_, and do not
rely on custom tools for this, but simply invoke the `git subtree add` command,
which adds the metadata to the comments in merge commits it makes that the other
`git subtree` commands and splitsh-lite can then use.

## addtomono.sh

The `git subtree add` command or something similar is required to graft new
subtrees into the _monorepo_ directory tree in a way that allows splitsh-lite to
maintain the _manyrepos_. The addtomono.sh script gives developers an easy way
to do this by inserting placeholder files into the _monorepo_ directory tree
where it should add the subtrees (provided the corresponding freestanding
repositories already exist with similar names).

## splitmono.sh

The actual work of splitting subtrees within a single remote repository happens
in `splitmono.sh`. It assumes that it has access to an ephemeral clone of the
_monorepo_ and uses splitsh-lite to create the branches within this for the
individual _manyrepos_. It then pushes these branches to the remote repositories
that are supposed to contain freestanding versions of these splits.

## tagsplits.sh

The splitmono.sh script does not copy new tags from the _monorepo_ to the
_manyrepos_ repositories, so this operation needs special handling. In addition
to propagating the tags themselves, this script updates the changelog files with
the repository-specific histories of the commits since the previous tag.

## Summary of Environment Variables in Shell Scripts

- `UAQSRTOOLS_ADDPRFIX` prefix for subtree addition placeholder filenames.
- `UAQSRTOOLS_EXEPATH` directory holding the splitting executable.
- `UAQSRTOOLS_LASTTAG` previously applied tag, as a stopping point for changelogs.
- `UAQSRTOOLS_MAGICTEXT` prefix for messages on commits to be tagged.
- `UAQSRTOOLS_MAINBRANCH` the main branch in the Git repositories.
- `UAQSRTOOLS_MONOCLONE` a directory holding an ephemeral clone of the _monorepo_.
- `UAQSRTOOLS_MONOREMOTE` short Git remote name for the remote _monorepo_.
- `UAQSRTOOLS_NEXTTAG` new tag to apply across all the repositories.
- `UAQSRTOOLS_OURNAME` name for the _monorepo_.
- `UAQSRTOOLS_PATHLIST` subpaths within the _monorepo_ to search for subtrees.
- `UAQSRTOOLS_QUIET` suppress logging messages.
- `UAQSRTOOLS_REPOSTEM` the start of the URLs for the _manyrepos_.
- `UAQSRTOOLS_REPOSUFFIX` generally `.git` for remote, or `/.git` for local repositories.
- `UAQSRTOOLS_SPLITTER` the name of the splitting program.
- `UAQSRTOOLS_WORKINGBRANCH` branch on which to make changes such as subtree additions.
